Program Juego;

uses
  crt;

const
  BG = 0;
  S = 2;
  Text = 15;
  gridX = 12;
  gridY = 12;
  refreshrate = 10;
var
  grid: array[1..gridX, 1..gridY] of integer;
  LC, HS: integer; //Living cells, Total cells
//data : text;

{procedure read_data;
var
line, t : string;
i ,t1: integer;
a, b : integer;
begin
assign(data, 'JDLV.txt');
reset(data);
i:= 1;
a := 0;
b := 0;
while not eof do 
begin
readln(data, line);
if length(line)=gridx*2 then begin
a := a+1;
for t1:=2 to gridx*2 do begin
t:= copy(line,t1,t1);
if t<>',' then begin
b:= b+1;
if t = '1' then grid[t1, i]:= 1;
if t = '0' then grid[t1, i]:= 0;
end;
end;
end;
end;
end;
}
//No funcional

procedure mapa1;
begin
  grid[3, 5] := 1;
  grid[4, 3] := 1;
  grid[5, 4] := 1;
  grid[2, 6] := 1;
  grid[7, 3] := 1;
  grid[6, 3] := 1;
  grid[6, 2] := 1;
  grid[6, 4] := 1;
  grid[6, 5] := 1;
  grid[7, 2] := 1;
  grid[7, 3] := 1;
  grid[7, 4] := 1;
end;

procedure drawgrid;
var
  tX, tY: integer;
begin
  LC := 0;
  textcolor(Text);
  textbackground(BG);
  for tY := 1 to GridY do
    for tX := 1 to GridX do
    begin
      //write('[');
      if grid[tX, tY] <> 404 then
        case grid[tX, tY] of
          1:
          begin
            textcolor(S);
            textbackground(S);
            write('1 ');
            textcolor(Text);
            textbackground(BG);
            LC := LC + 1;
          end;
          0: write('0 ');

        end;
      //write(']');
      if ty <> 1 then
        if tX = GridX then
          writeln;
    end;

end;

function Vec(aX, aY: integer): integer;
begin
  vec := 0;
  if grid[ax, ay] <> 404 then
  begin
    if grid[ax + 1, ay] = 1 then
      vec := vec + 1;
    if grid[ax - 1, ay] = 1 then
      vec := vec + 1;
    if grid[ax, ay + 1] = 1 then
      vec := vec + 1;
    if grid[ax, ay - 1] = 1 then
      vec := vec + 1;
    if grid[ax - 1, ay + 1] = 1 then
      vec := vec + 1;
    if grid[ax + 1, ay - 1] = 1 then
      vec := vec + 1;
    if grid[ax + 1, ay + 1] = 1 then
      vec := vec + 1;
    if grid[ax - 1, ay - 1] = 1 then
      vec := vec + 1;
  end;
end;

procedure borders;
var
  tX, tY: integer;
begin
  for tX := 1 to GridX do
    grid[tX, 1] := 404;
  for tX := 1 to GridX do
    grid[tX, gridY] := 404;
  for tY := 1 to GridY do
    grid[1, tY] := 404;
  for tY := 1 to GridY do
    grid[GridX, tY] := 404;
end;

procedure GOL;
var
  a, b: integer;
begin
  for b := 2 to GridY - 1 do
    for a := 2 to GridX - 1 do
      case grid[a, b] of
        1: if vec(a, b) = 2 then
            grid[a, b] := 1
          else
          if vec(a, b) < 2 then
            grid[a, b] := 0
          else
          if vec(a, b) = 3 then
            grid[a, b] := 1
          else
          if vec(a, b) > 3 then
            grid[a, b] := 0;
        0: if vec(a, b) = 3 then
            grid[a, b] := 1;
      end;
end;

procedure play;
var
  turno, op1, i: integer;
begin
  turno := 1;
  mapa1;
  repeat
    clrscr;
    writeln('Turno #', turno);
    writeln('Puntaje actual ', LC);
    writeln;
    drawgrid;
    writeln;
    for i := 1 to gridx - 1 do
      write('_ ');
    writeln('_ ');
    writeln('1 = Continuar');
    writeln('0 = Finalizar juego');
    readln(op1);
    case op1 of
      1:
      begin
        turno := turno + 1;
        gol;
      end;
      0:
      begin
        writeln('Finalizando juego');
        readln;
      end;
    end;
  until op1 = 0;
  clrscr;
  writeln('Puntaje Final : ', LC);
  if LC > HS then
    HS := LC;
  writeln('Puntaje mas alto : ', HS);
  readln;
end;

procedure highscore;
begin
  clrscr;
  writeln('Puntaje mas alto : ', HS);
  readln;
end;

procedure menu1;
var
  op1: integer;
begin
  repeat
    clrscr;
    writeln('Bienvenido');
    writeln('1.Jugar');
    writeln('2.Ver puntaje mas alto');
    writeln;
    writeln('9.Salir');
    readln(op1);
    case op1 of
      1: play;
      2: highscore;
    end;
  until op1 = 9;
  clrscr;
  writeln('Gracias por jugar');
  readln;
end;

begin
  borders;
  clrscr;
  menu1;
end.
